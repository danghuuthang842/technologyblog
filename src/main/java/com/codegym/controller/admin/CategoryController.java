package com.codegym.controller.admin;


import com.codegym.model.Category;
import com.codegym.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class CategoryController extends AdminBaseController{

    private final String TERM = "Category Manager";

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/category/")
    public ModelAndView index() {
        Iterable<Category> categories = categoryService.findAll();
        ModelAndView modelAndView = new ModelAndView("admin/category/index");
        modelAndView.addObject("categories", categories);
        modelAndView.addObject("title", TITLE_ADD);
        modelAndView.addObject("term", TERM);

        return modelAndView;
    }
    @GetMapping("/category/add")
    public ModelAndView showAddForm() {
        ModelAndView modelAndView = new ModelAndView("admin/category/add");
        modelAndView.addObject("category", new Category());
        modelAndView.addObject("action", ACTION_ADD);
        modelAndView.addObject("term", TERM);
        modelAndView.addObject("title", TITLE_ADD);

        return modelAndView;
    }

    @PostMapping("/category/add")
    public ModelAndView saveAddForm(HttpServletRequest request, @ModelAttribute("category") Category category) {

        categoryService.save(category);

        ModelAndView modelAndView = new ModelAndView("admin/category/add");
        modelAndView.addObject("category", new Category());

        modelAndView.addObject("action", ACTION_ADD);
        modelAndView.addObject("term", TERM);
        modelAndView.addObject("title", TITLE_ADD);
        modelAndView.addObject("alert", ALERT_SUCCESS);

        modelAndView.addObject("message", ACTION_ADD_SUCCESS);

        return modelAndView;
    }
    @PostMapping("/category/edit")
    public ModelAndView saveEditForm(HttpServletRequest request, @ModelAttribute("category") Category category) {

        //
        categoryService.save(category);
        //
        ModelAndView modelAndView = new ModelAndView("admin/category/add");
        modelAndView.addObject("category", category);
        modelAndView.addObject("action", ACTION_EDIT);
        modelAndView.addObject("term", TERM);
        modelAndView.addObject("title", TITLE_EDIT);
        modelAndView.addObject("alert", ALERT_SUCCESS);
        modelAndView.addObject("message", ACTION_EDIT_SUCCESS);
        //
        return modelAndView;
    }

    @GetMapping("/category/edit/{id}")
    public ModelAndView showUpdateForm(@PathVariable Long id) {
        Category category = categoryService.findById(id);
        if (category != null) {

            ModelAndView modelAndView = new ModelAndView("admin/category/add");
            modelAndView.addObject("category", category);
            modelAndView.addObject("action", ACTION_EDIT);
            modelAndView.addObject("term", TERM);
            modelAndView.addObject("title", TITLE_EDIT);

            return modelAndView;

        } else {
            ModelAndView modelAndView = new ModelAndView("/error.404");
            return modelAndView;
        }

    }

    @GetMapping("/category/delete/{id}")
    public ModelAndView showDeleteForm(@PathVariable Long id) {
        Category category = categoryService.findById(id);
        if (category != null) {
            ModelAndView modelAndView = new ModelAndView("admin/category/delete");

            modelAndView.addObject("category", category);
            modelAndView.addObject("action", ACTION_DELETE);
            modelAndView.addObject("term", TERM);
            modelAndView.addObject("title", TITLE_DELETE);
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("/error.404");
            return modelAndView;
        }
    }

    @PostMapping("/category/delete")
    public String deleteContent(@ModelAttribute("category") Category category){
        categoryService.remove(category.getId());
        return "redirect:/admin/category/";
    }
}
