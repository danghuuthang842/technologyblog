package com.codegym.controller.admin;


import com.codegym.model.Message;
import com.codegym.service.MessageService;
import com.codegym.service.PostContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Set;

@RestController
@RequestMapping("/api")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @Autowired
    private PostContentService postContentService;

    @GetMapping(value = "/messages/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<Message>> getMessage(@PathVariable("id") long id) {

        Set<Message> message = postContentService.findById(id).getMessages();

        return new ResponseEntity<Set<Message>>(message, HttpStatus.OK);
    }

    @PostMapping(value = "/messages/")
    public ResponseEntity<Message> createMessage(@RequestBody Message message, UriComponentsBuilder ucBuilder) {

        message.setPostContent(postContentService.findById(message.getPostContent().getId()));
        try {
            messageService.save(message);

            return new ResponseEntity<Message>(message, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<Message>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
