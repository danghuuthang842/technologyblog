//package com.codegym.controller.admin;
//
//import com.codegym.model.Tag;
//import com.codegym.service.TagService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.List;
//
//
//
//@Controller
//@RequestMapping("/admin")
//public class TagController extends AdminBaseController{
//    private final String TERM = "Tag Manager";
//
//    @Autowired
//    private TagService tagSevice;
//
//    @GetMapping("/tag/")
//    public ModelAndView index() {
//        Iterable<Tag> tags = tagSevice.findAll();
//        ModelAndView modelAndView = new ModelAndView("/admin/tag/index");
//        modelAndView.addObject("tag", tags);
//        modelAndView.addObject("title", TITLE_ADD);
//        modelAndView.addObject("term", TERM);
//
//        return modelAndView;
//    }
//    @GetMapping("/tag/add")
//    public ModelAndView showAddForm() {
//        ModelAndView modelAndView = new ModelAndView("/admin/tag/add");
//        modelAndView.addObject("tag", new Tag());
//        modelAndView.addObject("action", ACTION_ADD);
//        modelAndView.addObject("term", TERM);
//        modelAndView.addObject("title", TITLE_ADD);
//
//        return modelAndView;
//    }
//
//    @PostMapping("/tag/add")
//    public ModelAndView saveAddForm(HttpServletRequest request, @ModelAttribute("tag") Tag tag) {
//
//        tagSevice.save(tag);
//
//        ModelAndView modelAndView = new ModelAndView("/admin/tag/add");
//        modelAndView.addObject("tags", new Tag());
//
//        modelAndView.addObject("action", ACTION_ADD);
//        modelAndView.addObject("term", TERM);
//        modelAndView.addObject("title", TITLE_ADD);
//        modelAndView.addObject("alert", ALERT_SUCCESS);
//
//        modelAndView.addObject("message", ACTION_ADD_SUCCESS);
//
//        return modelAndView;
//    }
//    @PostMapping("/tag/edit/{id}")
//    public ModelAndView saveEditForm(HttpServletRequest request, @ModelAttribute("tag") Tag tag) {
//
//        //
//        tagSevice.save(tag);
//        //
//        ModelAndView modelAndView = new ModelAndView("/admin/tag/add");
//        modelAndView.addObject("tag", tag);
//        modelAndView.addObject("action", ACTION_EDIT);
//        modelAndView.addObject("term", TERM);
//        modelAndView.addObject("title", TITLE_EDIT);
//        modelAndView.addObject("alert", ALERT_SUCCESS);
//        modelAndView.addObject("message", ACTION_EDIT_SUCCESS);
//        //
//        return modelAndView;
//    }
//
//    @GetMapping("/tag/edit/{id}")
//    public ModelAndView showUpdateForm(@PathVariable Long id) {
//        Tag tag = tagSevice.findById(id);
//        if (tag != null) {
//
//            ModelAndView modelAndView = new ModelAndView("/admin/tag/add");
//            modelAndView.addObject("tag", tag);
//            modelAndView.addObject("action", ACTION_EDIT);
//            modelAndView.addObject("term", TERM);
//            modelAndView.addObject("title", TITLE_EDIT);
//
//            return modelAndView;
//
//        } else {
//            ModelAndView modelAndView = new ModelAndView("/error.404");
//            return modelAndView;
//        }
//
//    }
//
//    @GetMapping("/tag/delete/{id}")
//    public ModelAndView showDeleteForm(@PathVariable Long id) {
//        Tag tag = tagSevice.findById(id);
//        if (tag != null) {
//            ModelAndView modelAndView = new ModelAndView("/admin/tag/delete");
//
//            modelAndView.addObject("tag", tag);
//            modelAndView.addObject("action", ACTION_DELETE);
//            modelAndView.addObject("term", TERM);
//            modelAndView.addObject("title", TITLE_DELETE);
//            return modelAndView;
//        } else {
//            ModelAndView modelAndView = new ModelAndView("/error.404");
//            return modelAndView;
//        }
//    }
//
//    @PostMapping("/tag/delete")
//    public String deleteContent(@ModelAttribute("tag") Tag tag){
//        tagSevice.remove(tag.getId());
//        return "redirect:/admin/tag/";
//    }
//}
