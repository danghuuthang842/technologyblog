package com.codegym.controller.user;

import com.codegym.model.PostContent;
import com.codegym.repository.PostContentRepository;
import com.codegym.service.PostContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/category")
public class UserCategoryController {

    @Autowired
    private PostContentService postContentService;
    @Autowired
    private PostContentRepository postContentRepository;
    public static final String TECH = "1";
    public static final String CODE = "2";
    public static final String TOOLS = "3";
    @GetMapping("/{category}")
    public ModelAndView tech(@PathVariable("category") String category){
        List<PostContent> findmostview = postContentService.findmostview();
        Page<PostContent> findnewpostby5 = postContentRepository.findnewpostby5(PageRequest.of(0,4));
        ModelAndView modelAndView = new ModelAndView("/user/category");
        modelAndView.addObject("findmostview",findmostview);
        modelAndView.addObject("findnewpostby5",findnewpostby5);
        switch (category)
        {
            case "tech":
            case TECH:
                List<PostContent> categoriesById1 = postContentService.findCategoriesById();
                modelAndView.addObject("tech",categoriesById1);
                modelAndView.addObject("category", categoriesById1.get(0).getCategory());
                return modelAndView;
            case "code":
            case CODE:
                List<PostContent> categoriesById2 = postContentService.categorybyid2();
                modelAndView.addObject("category", categoriesById2.get(0).getCategory());
                modelAndView.addObject("code",categoriesById2);
                return modelAndView;
            case "tools":
            case TOOLS:
                List<PostContent> categoriesById3 = postContentService.categorybyid3();
                modelAndView.addObject("category", categoriesById3.get(0).getCategory());
                modelAndView.addObject("tools",categoriesById3);
                return modelAndView;
        }
        return modelAndView;

    }

}
