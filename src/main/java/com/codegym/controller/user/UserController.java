package com.codegym.controller.user;

import com.codegym.model.PostContent;
import com.codegym.model.Tag;
import com.codegym.repository.PostContentRepository;
import com.codegym.service.CategoryService;
import com.codegym.service.PostContentService;
import com.codegym.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class UserController {
    @Autowired
    private PostContentService postContentService;

    @Autowired
    private PostContentRepository postContentRepository;

    @Autowired
    private TagService tagService;


    @Autowired
    private CategoryService categoryService;

    @GetMapping("/")
    public ModelAndView dashboard(@RequestParam(defaultValue = "0") int page){
        ModelAndView modelAndView = new ModelAndView("/user/index");
        List<PostContent> postContentList = postContentService.findPostLimit();

        List<PostContent> contentList =postContentService.findCategoriesById();
        List<PostContent> slidePost = new ArrayList<>();
        List<PostContent> haithangcuoi = new ArrayList<>();
        for (int i =0; i<contentList.size(); i++) {
            if(i<3){
                slidePost.add(contentList.get(i));
            }else {
                if (haithangcuoi.size() < 4) {
                    haithangcuoi.add(contentList.get(i));
                }
                else {
                    break;
                }
            }
        }

        List<PostContent> categoriesbyid2 = postContentService.categorybyid2();
        List<PostContent> first3post = new ArrayList<>();
        List<PostContent> last2post = new ArrayList<>();
        for (int i =0; i<categoriesbyid2.size(); i++) {
            if(i<3){
                first3post.add(categoriesbyid2.get(i));
            }else {
                if (last2post.size() < 4) {
                    last2post.add(contentList.get(i));
                }
                else {
                    break;
                }
            }
        }
        List<PostContent> categoriesbyid3 = postContentService.categorybyid3();
        List<PostContent> first3postid3 = new ArrayList<>();
        List<PostContent> last2postid3 = new ArrayList<>();
        for (int i =0; i<categoriesbyid3.size(); i++) {
            if(i<3){
                first3postid3.add(categoriesbyid3.get(i));
            }else {
                if (last2postid3.size() < 4) {
                    last2postid3.add(contentList.get(i));
                }
                else {
                    break;
                }
            }
        }
        postContentList.remove(0);
//        Page<PostContent> findallbylimit = postContentService.findAll(PageRequest.of(0,5));
        Page<PostContent> newpost3 = postContentRepository.findByPostOrderByDateDesc(PageRequest.of(0,2));
//        List<PostContent> findbytitle = postContentRepository.findByPostOrderByDateDesc1();



        List<PostContent> findall =postContentService.finddnew();

        findall.remove(1);
        findall.remove(0);





        Page<PostContent> PageSort = postContentRepository.page(PageRequest.of(page,4));



        List<PostContent> findmostview = postContentService.findmostview();

        Page<PostContent> findnewpostby5 = postContentRepository.findnewpostby5(PageRequest.of(0,4));
        Page<PostContent> findtrendingpost = postContentRepository.findtrendingpost(PageRequest.of(0,3));

        modelAndView.addObject("postHeader",postContentService.findnewPost());
        modelAndView.addObject("postHome",postContentList);
        modelAndView.addObject("postcontentbycate",slidePost);
        modelAndView.addObject("postcontentbylast",haithangcuoi);
        modelAndView.addObject("first3post",first3post);
        modelAndView.addObject("last2post",last2post);
        modelAndView.addObject("first3postid3",first3postid3);
        modelAndView.addObject("last2postid3",last2postid3);
        modelAndView.addObject("newpost3",newpost3);
        modelAndView.addObject("findall",findall);
        modelAndView.addObject("findmostview",findmostview);
        modelAndView.addObject("findnewpostby5",findnewpostby5);
        modelAndView.addObject("findtrendingpost",findtrendingpost);
        modelAndView.addObject("data",PageSort);
//        modelAndView.addObject("data",postContentRepository.page(PageRequest.of(page,4)));


        return modelAndView;
    }

    @GetMapping("/contact")
    public ModelAndView contact(){

        ModelAndView modelAndView = new ModelAndView("/user/contact");
        return modelAndView;
    }
    @GetMapping("/login")
    public ModelAndView login(){

        ModelAndView modelAndView = new ModelAndView("/user/login");
        return modelAndView;
    }
    @GetMapping("/view/{id}")
    public ModelAndView dashboard1(@PathVariable("id") Long id){

        ModelAndView modelAndView = new ModelAndView("/user/single-post");

        PostContent postContents = postContentService.findById(id);
        postContents.setNumberView(postContents.getNumberView()+1);
        List<Tag> tagList = tagService.findTagByContentId(id);
        postContentService.save(postContents);
        modelAndView.addObject("postContent", postContents);
        modelAndView.addObject("tag",tagList);
        modelAndView.addObject("views",postContents.getNumberView());
        return modelAndView;
    }
}