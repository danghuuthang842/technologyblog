//package com.codegym.controller.user;
//
//import com.codegym.model.PostContent;
//import com.codegym.model.Tag;
//import com.codegym.service.PostContentService;
//import com.codegym.service.TagService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.util.UriComponentsBuilder;
//
//import java.util.List;
//import java.util.Set;
//
//@Controller
//@RequestMapping("/")
//public class UserController {
//    @Autowired
//    private PostContentService postContentService;
//
//    @Autowired
//    private TagService tagService;
//
//
//    @GetMapping("/")
//    public ModelAndView dashboard(){
//        ModelAndView modelAndView = new ModelAndView("/user/index");
//        List<PostContent> postContentList = postContentService.findPostLimit();
//        postContentList.remove(0);
//        modelAndView.addObject("postHeader",postContentService.findnewPost());
//        modelAndView.addObject("postHome",postContentList);
//        return modelAndView;
//    }
//
//    @GetMapping("/view/{id}")
//    public ModelAndView dashboard1(@PathVariable Long id){
//
//        ModelAndView modelAndView = new ModelAndView("/user/single-post");
//
//       PostContent postContents = postContentService.findById(id);
//        List<Tag> tagList = tagService.findTagByContentId(id);
//        modelAndView.addObject("postContent", postContents);
//        modelAndView.addObject("tag",tagList);
//        return modelAndView;
//    }
//
////    @GetMapping(value = "/messages/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
////    public ResponseEntity<Set<Message>> getMessage(@PathVariable("id") long id) {
////        Set<Message> message = postContentService.findById(id).getMessages();
////
////        return new ResponseEntity<Set<Message>>(message, HttpStatus.OK);
////    }
////    @PostMapping(value = "/messages/")
////    public ResponseEntity<Message> createMessage(@RequestBody Message message, UriComponentsBuilder ucBuilder) {
////
////        message.setPostContent(postContentService.findById(message.getPostContent().getId()));
////        try {
////            messageService.save(message);
////
////            return new ResponseEntity<Message>(message, HttpStatus.OK);
////        } catch (Exception ex) {
////            return new ResponseEntity<Message>(HttpStatus.INTERNAL_SERVER_ERROR);
////        }
////    }
//}
