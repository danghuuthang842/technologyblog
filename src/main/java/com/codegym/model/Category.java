package com.codegym.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "categorys")
public class Category {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition="varchar(500)")
    private String title;

    //    @ManyToOne
//    @JoinColumn(name="post_catagory")
//    private PostContent postContent;


    @JsonIgnore
    @OneToMany(mappedBy = "category", fetch = FetchType.EAGER)
    private Set<PostContent> postContents;

    public Set<PostContent> getPostContents() {
        return postContents;
    }

    public void setPostContents(Set<PostContent> postContents) {
        this.postContents = postContents;
    }


    public Category() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
