package com.codegym.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import net.bytebuddy.implementation.bind.annotation.Default;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Where;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "postcontents")
@Where(clause = "isDelete = 0")
public class PostContent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition="varchar(500)")
    private String title;
    @ColumnDefault("0")
    private Long numberView;
    @ColumnDefault("0")
    private Long numberLike;

    public Long getNumberView() {
        return numberView;
    }

    public void setNumberView(Long numberView) {
        this.numberView = numberView;
    }

    public Long getNumberLike() {
        return numberLike;
    }

    public void setNumberLike(Long numberLike) {
        this.numberLike = numberLike;
    }

    @Column(columnDefinition="LONGTEXT")
    private String description;

    @Column(columnDefinition="LONGTEXT")
    private String content;

    private String image;

    public LocalDateTime getDate() {
        return Date;
    }

    public void setDate(LocalDateTime date) {
        Date = date;
    }

    @Column(columnDefinition="DATETIME")
    private LocalDateTime Date;

    private short isDelete;

    @ManyToOne
    @JoinColumn(name = "categories_id")
    public Category category;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @JsonIgnore
    @OneToMany(mappedBy = "postContent",fetch = FetchType.EAGER)
    public Set<Post_Tag> post_tags;

    public Set<Post_Tag> getPost_tags() {
        return post_tags;
    }


    @JsonIgnore
    @OneToMany(mappedBy = "postContent",fetch = FetchType.EAGER)
    public Set<Message> messages;

    public Set<Message> getMessages() {
        return messages;
    }

    public void setMessages(Set<Message> messages) {
        this.messages = messages;
    }


    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Transient
    private Set<Tag> tags;

    public void setPost_tags(Set<Post_Tag> post_tags) {
        this.post_tags = post_tags;
    }

    public CommonsMultipartFile[] getFileImage() {
        return fileImage;
    }

    public void setFileImage(CommonsMultipartFile[] fileImage) {
        this.fileImage = fileImage;
    }

    @Transient
    @JsonIgnore
    private CommonsMultipartFile[] fileImage;

    public PostContent() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}