package com.codegym.repository;

import com.codegym.model.Category;
import com.codegym.model.PostContent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PostContentRepository extends JpaRepository<PostContent,Long> {

    @Override
    @Modifying
    @Query("update PostContent p set p.isDelete=1 where p.id=:id")
    void deleteById(@Param("id") Long id);

    @Query(value="SELECT * FROM postcontents order by postcontents.id desc limit 1;",nativeQuery=true)
    PostContent findId();

    @Query(value="SELECT * FROM postcontents ORDER BY date DESC LIMIT 1", nativeQuery = true)
    PostContent findnewPost();

    @Query(value="SELECT * FROM postcontents ORDER BY date DESC LIMIT 6", nativeQuery = true)
    List<PostContent> findPostLimit();

    @Query(value="SELECT * FROM postcontents WHERE categories_id=1 ORDER BY date DESC", nativeQuery = true)
    List<PostContent> findCategoriesById();

    @Query(value="SELECT * FROM postcontents WHERE categories_id=2 ORDER BY date DESC", nativeQuery = true)
    List<PostContent> categorybyid2();

    @Query(value="SELECT * FROM postcontents WHERE categories_id=3 ORDER BY date DESC", nativeQuery = true)
    List<PostContent> categorybyid3();

    @Query(value="SELECT * FROM postcontents ORDER BY date DESC", nativeQuery = true)
    Page<PostContent> findByPostOrderByDateDesc(PageRequest of);

    @Query(value="SELECT * FROM postcontents ORDER BY date DESC", nativeQuery = true)
    List<PostContent> finddnew();

    @Query(value="SELECT * FROM postcontents ORDER BY numberView DESC LIMIT 5;", nativeQuery = true)
    List<PostContent> findmostview();

    @Query(value="SELECT * FROM postcontents ORDER BY date DESC", nativeQuery = true)
    Page<PostContent> findnewpostby5(PageRequest of);

    @Query(value="SELECT * FROM postcontents ORDER BY numberLike DESC", nativeQuery = true)
    Page<PostContent> findtrendingpost(PageRequest of);

    @Query(value="SELECT * FROM postcontents ORDER BY date DESC", nativeQuery = true)
    Page<PostContent> page(PageRequest of);


}
