package com.codegym.service;

import java.util.List;

public interface IService<T> {

    List<T> findAll();

    T findById(Long id);

    void save(T Object);

    void remove(Long id);


}
