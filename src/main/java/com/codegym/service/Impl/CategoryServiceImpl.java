package com.codegym.service.Impl;

import com.codegym.model.Category;
import com.codegym.model.PostContent;
import com.codegym.repository.CategoryRepository;
import com.codegym.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CategoryServiceImpl implements CategoryService {


    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findById(Long id) {
        return categoryRepository.findById(id).get();
    }

    @Override
    public void save(Category Object) {
        categoryRepository.save(Object);
    }

    @Override
    public void remove(Long id) {
        categoryRepository.deleteById(id);
    }

//    @Override
//    public List<Category> findCategoriesById() {
//        return categoryRepository.findCategoriesById();
//    }
}
