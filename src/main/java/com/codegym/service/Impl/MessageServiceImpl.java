package com.codegym.service.Impl;

import com.codegym.model.Message;
import com.codegym.repository.MessageRepository;
import com.codegym.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageRepository messageRepository;

    @Override
    public List<Message> findAll() {
        return messageRepository.findAll();
    }

    @Override
    public Message findById(Long id) {
        return messageRepository.findById(id).get();
    }

    @Override
    public void save(Message Object) {
        messageRepository.save(Object);
    }

    @Override
    public void remove(Long id) {
        messageRepository.deleteById(id);
    }
}
