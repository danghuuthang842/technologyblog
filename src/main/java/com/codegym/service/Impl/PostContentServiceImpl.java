package com.codegym.service.Impl;

import com.codegym.model.PostContent;
import com.codegym.repository.PostContentRepository;
import com.codegym.service.PostContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

public class PostContentServiceImpl implements PostContentService {

        @Autowired
        private PostContentRepository postContentRepository;

    @Override
    public List<PostContent> findAll() {
        return postContentRepository.findAll();
    }

    @Override
    public PostContent findById(Long id) {
        return postContentRepository.findById(id).get();
    }

    @Override
    public void save(PostContent Object) {
            postContentRepository.save(Object);
    }

    @Override
    public void remove(Long id) {
            postContentRepository.deleteById(id);
    }

    @Override
    public PostContent findId() {
        return postContentRepository.findId();
    }

    @Override
    public Page<PostContent> findAll(Pageable pageable) {
        return postContentRepository.findAll(pageable);
    }


    @Override
    public PostContent findnewPost() {
        return postContentRepository.findnewPost();
    }

    @Override
    public List<PostContent> findPostLimit() {
        return postContentRepository.findPostLimit();
    }

    @Override
    public List<PostContent> findCategoriesById() {
        return postContentRepository.findCategoriesById();
    }

    @Override
    public List<PostContent> categorybyid2() {
        return postContentRepository.categorybyid2();
    }

    @Override
    public List<PostContent> categorybyid3() {
        return postContentRepository.categorybyid3();
    }

    @Override
    public List<PostContent> findByPostOrderByDateDesc1() {
        return null;
    }

    @Override
    public List<PostContent> finddnew() {
        return postContentRepository.finddnew();
    }

    @Override
    public List<PostContent> findmostview() {
        return postContentRepository.findmostview();
    }


}
