package com.codegym.service.Impl;

import com.codegym.model.Tag;
import com.codegym.repository.TagRepository;
import com.codegym.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class TagServiceImpl implements TagService {

    @Autowired
    private TagRepository tagRepository;

    @Override
    public List<Tag> findAll() {
        return tagRepository.findAll();
    }

    @Override
    public Tag findById(Long id) {
        return tagRepository.findById(id).get();
    }

    @Override
    public void save(Tag Object) {
        tagRepository.save(Object);
    }

    @Override
    public void remove(Long id) {
        tagRepository.deleteById(id);
    }

    @Override
    public List<Tag> findTagByContentId(Long idPostContent) {
        return tagRepository.findTagByContentId(idPostContent);
    }
}
