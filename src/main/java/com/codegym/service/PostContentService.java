package com.codegym.service;

import com.codegym.model.PostContent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PostContentService extends IService<PostContent> {

    PostContent findId();

    Page<PostContent> findAll(Pageable pageable);

    PostContent findnewPost();

    List<PostContent> findPostLimit();
    List<PostContent> findCategoriesById();
    List<PostContent> categorybyid2();
    List<PostContent> categorybyid3();
    List<PostContent> findByPostOrderByDateDesc1();
    List<PostContent> finddnew();

    List<PostContent> findmostview();
}
