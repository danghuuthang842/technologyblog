package com.codegym.service;

import com.codegym.model.Post_Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface Post_TagService extends IService<Post_Tag> {

    Post_Tag findByIdByPost_tag(@Param("idP") Long idP, @Param("idT") Long idT);

}
