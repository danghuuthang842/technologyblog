package com.codegym.service;

import com.codegym.model.Tag;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TagService extends IService<Tag> {
    List<Tag> findTagByContentId(@Param("idPostContent") Long idPostContent);
}
